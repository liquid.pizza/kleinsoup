# set base image (host OS)
FROM python:3.10

ENV POETRY_VERSION=1.3.2

# set the working directory in the container
WORKDIR /code

# install poetry
#RUN curl -sSL https://install.python-poetry.org | python3 -
RUN pip install "poetry==$POETRY_VERSION"

# copy the content of the local src directory to the working directory
COPY ./ .

RUN poetry install

# command to run on container start
ENTRYPOINT ["poetry", "run", "deploy"]
