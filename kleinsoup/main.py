from bs4 import BeautifulSoup
import requests
import re

import pandas as pd
from dash import Dash, html, dcc, Input, Output
import plotly.express as px

from pprint import pprint as pp

from dataclasses import dataclass

REQUEST_URL = "https://www.google.com/search?hl=de&q=beautiful%20soup"
REQUEST_URL = "https://www.ebay-kleinanzeigen.de"
REQUEST_URL = "https://www.golem.de"
REQUEST_URL = "https://www.ebay-kleinanzeigen.de/s-notebooks/framework/k0c278"
REQUEST_URL = "https://www.ebay-kleinanzeigen.de/s-lego-76240/k0"
REQUEST_URL = "https://www.ebay-kleinanzeigen.de/s-kah647pl/k0"

LONG_URL = "https://www.ebay-kleinanzeigen.de/s-suchanfrage.html?keywords=kah647pl&categoryId=&locationStr=&locationId=&radius=0&sortingField=SORTING_DATE&adType=&posterType=&pageNum=1&action=find&maxPrice=&minPrice=&buyNowEnabled=false"

SEARCH_STR = "kah647pl"
BASE_URL = "https://www.ebay-kleinanzeigen.de"

COOKIES = {
    "_uetvid": "62abf9a09e1b11ed8b07eb10c5a77b25",
    "ekConsentTcf2": "{%22customVersion%22:5%2C%22encodedConsentString%22:%22CPmQ1u_PmQ1u_E1ABADECQCgAP_AAAAAAAYgIxNd_X__bX9n-_7_7ft0eY1f9_r3_-QzjhfNs-8F3L_W_L0X32E7NF36tq4KuR4ku3bBIQNtHMnUTUmxaolVrzHsak2cpyNKJ7LkknsZe2dYGH9Pn9lD-YKZ7_5___f53T___9_-39z3_9f___d__-__-vjf_599n_v9fV_7___________-_________wAAAEhIAMAAQRiDQAYAAgjEIgAwABBGIVABgACCMQyADAAEEYh0AGAAIIxEIAMAAQRiJQAYAAgjEUgAwABBGIA.YAAAAAAAA6sA%22%2C%22googleConsentGiven%22:true%2C%22consentInterpretation%22:{%22googleAdvertisingFeaturesAllowed%22:true%2C%22googleAnalyticsAllowed%22:true%2C%22infonlineAllowed%22:true%2C%22theAdexAllowed%22:true%2C%22snowplowAllowed%22:true%2C%22criteoAllowed%22:true}}",
    "ekConsentBucketTcf2": "full2-exp",
    "_uetsid": "62aa56e09e1b11eda86b675bdbab637f",

}

app = Dash(
    __name__,
    external_stylesheets=['https://codepen.io/chriddyp/pen/bWLwgP.css'],
)

@app.callback(
    Output("visulaization", "children"),
    Input("input_keyword", "value")
)
def visualize_data_by_path(key_word):
    return visualize_data(key_word)

def gen_url(key_word: str):
    print(f"gen_url {key_word}")
    return f"https://www.ebay-kleinanzeigen.de/s-suchanfrage.html?keywords={key_word}&categoryId=&locationStr=&locationId=&radius=0&sortingField=SORTING_DATE&adType=&posterType=&pageNum=1&action=find&maxPrice=&minPrice=&buyNowEnabled=false"


def gen_layout():

    return html.Div(
        children=[
            html.H1(children='Welcome to your kleinsoup'),
            html.Div(
                children=[
                    html.P(
                        "Enter search string"
                    ),
                    dcc.Input(
                        id="input_keyword",
                        type="search",
                        value=SEARCH_STR,
                        debounce=True,
                    ),
                ],
                className="ten columns",
            ),
            html.Div(
                id="visulaization",
            ),
        ],
        style={
            "text-align": "center",
            "padding": "5px",
        },
    )

def visualize_data(key_word: str = SEARCH_STR):
    df = scrape(gen_url(key_word))

    print(df)

    df_grouped_price = df.value_counts(
        "price",
        dropna=True,
    )

    return html.Div(
        children=[
            html.Div(
                children=[
                    html.Div(className="three columns"),
                    html.Div(
                        children=[
                            html.H3(children=f"{len(df)} listings"),
                            html.P("for"),
                            html.H3(f"{key_word}", style={"color": "darkgrey"}),
                            html.P("with mean price"),
                            html.H3(f"{round(df['price'].mean(), 2)} €")
                        ],
                        className="three columns",
                        ),
                    dcc.Graph(
                        id="price",
                        figure=px.bar(
                            df_grouped_price,
                            range_x=[0, df["price"].max() + df["price"].min()],
                        ),
                        className="eight columns",
                        animate=True,
                        responsive=True,
                    ),
                ],
            ),
        ],
        style={
            "text-align": "center",
            "padding": "5px",
        },
    )


@dataclass
class Listing:
    title: str
    link: str
    price: int
    vb: bool
    shipping: bool


def parse_price(price_str: str):
    price_str = price_str.strip()
    match = re.match(r"\d+", price_str)

    if match:
        return int(match.group())


def parse_vb(price_str: str):
    price_str = price_str.strip()
    match = re.match(r"VB", price_str)

    if match:
        return bool(match.group())


def scrape(url: str):
    s = requests.Session()
    # s.cookies.update({
    #     "ekConsentBucketTcf2": "full2-exp",
    #     "ekConsentTcf2": "{%22customVersion%22:5%2C%22encodedConsentString%22:%22CPmQ1u_PmQ1u_E1ABADECQCgAP_AAAAAAAYgIxNd_X__bX9n-_7_7ft0eY1f9_r3_-QzjhfNs-8F3L_W_L0X32E7NF36tq4KuR4ku3bBIQNtHMnUTUmxaolVrzHsak2cpyNKJ7LkknsZe2dYGH9Pn9lD-YKZ7_5___f53T___9_-39z3_9f___d__-__-vjf_599n_v9fV_7___________-_________wAAAEhIAMAAQRiDQAYAAgjEIgAwABBGIVABgACCMQyADAAEEYh0AGAAIIxEIAMAAQRiJQAYAAgjEUgAwABBGIA.YAAAAAAAA6sA%22%2C%22googleConsentGiven%22:true%2C%22consentInterpretation%22:{%22googleAdvertisingFeaturesAllowed%22:true%2C%22googleAnalyticsAllowed%22:true%2C%22infonlineAllowed%22:true%2C%22theAdexAllowed%22:true%2C%22snowplowAllowed%22:true%2C%22criteoAllowed%22:true}}",
    # })

    # print(f"scrape {url}")

    s.headers.update({
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; rv:91.0) Gecko/20100101 Firefox/91.0'
    })

    res = s.get(url, cookies=COOKIES)

    with open("test.html", "w") as f:
        f.write(res.text)

    # create a soup
    soup = BeautifulSoup(res.text, "html.parser")

    listings = soup.find_all(
        "article",
        class_="aditem",
    )

    listings_data = []

    print(f"{len(listings)} listings found")

    for l in listings:

        price_str = l.find(
            "p", class_="aditem-main--middle--price-shipping--price").text.strip()
        price = parse_price(price_str)
        vb = price_str.endswith("VB")

        zip_code = parse_price(
            l.find(
                "div",
                class_="aditem-main--top--left",
            ).text
        )

        data = {
            "title": l.find("a", class_="ellipsis").text,
            "link": BASE_URL + l.find("a", class_="ellipsis")["href"],
            "price": price,
            "vb": vb,
            "shipping": bool(l.find("p", class_="aditem-main--middle--price-shipping--shipping")),
            "zip_code": zip_code,
        }

        listings_data.append(data)

    return create_df(listings_data)


def create_df(listings: list):
    # print(f"create_df")
    df = pd.DataFrame.from_records(listings)
    # print(df)
    return df


def main():
    app.layout = gen_layout()
    app.run_server(host="0.0.0.0", debug=True)

def deploy():
    app.layout = gen_layout()
    app.run_server(host="0.0.0.0")

if __name__ == "__main__":
    main()
